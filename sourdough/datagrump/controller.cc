#include <iostream>

#include "controller.hh"
#include "timestamp.hh"

using namespace std;

/* Default constructor */
Controller::Controller( const bool debug )
  : debug_( debug ),
    acks_info( NULL ),
    newest_ack_received( NULL ),
    the_window_size( 25 ),
    num_recvd_acks ( 0 )
{}

/* Copy constructor */
/*
Controller::Controller( const Controller &obj ) {
  debug_ = obj.debug_;
  datagrams_info = obj.datagrams_info;
  newest_datagram_sent = obj.newest_datagram_sent;
  acks_map = obj.acks_map;
}
*/

Controller::Controller( const Controller &obj )
  : debug_( obj.debug_ ),
    acks_info( obj.acks_info ),
    newest_ack_received( obj.newest_ack_received ),
    the_window_size( obj.the_window_size ),
    num_recvd_acks( obj.num_recvd_acks )
{}

Controller& Controller::operator=( const Controller &other ) {
  debug_ = other.debug_;
  acks_info = other.acks_info;
  newest_ack_received = other.newest_ack_received;
  the_window_size = other.the_window_size;
  num_recvd_acks = other.num_recvd_acks;
  return *this;
}

/* Get current window size, in datagrams */
unsigned int Controller::window_size( void )
{
  /* Default: fixed window size of 100 outstanding datagrams */
  //unsigned int the_window_size = 25;
  int threshold = 150;

  unsigned int min_window = 25;
  /* If RTT takes longer than threshold, we should decrease window size
     Else, increase window size */
/*
  datagram_sent_info *oldest_datagram = datagrams_info;
  if (oldest_datagram != NULL) {
    uint64_t oldest_seq_no = oldest_datagram->sequence_number;

    if (acks_map.find(oldest_seq_no) != acks_map.end()) {
      ack_received_info *ack_found = acks_map[oldest_seq_no];
      int rtt = 2 * (ack_found->timestamp_ack_received - ack_found->send_timestamp_acked);// + 
                //recv_timestamp_acked - ack_found-> oldest_datagram->send_timestamp;
      if ( debug_ )
        //cerr << "time ack received" << ack_found->timestamp_ack_received << endl;
        cerr << "time RTT: " << rtt << "seqno: " << oldest_seq_no << endl;
      if (rtt > threshold) {
        the_window_size *= 0.5;
      } else {
        the_window_size += 1;
      }
      datagrams_info = oldest_datagram->next_datagram;
      free(oldest_datagram);
      acks_map.erase(oldest_seq_no);
    }
  }
*/
  ack_received_info *oldest_ack = acks_info;
  if (oldest_ack != NULL) {
    int rtt = oldest_ack->timestamp_ack_received - oldest_ack->send_timestamp_acked;// + 

    if ( debug_ )
      //cerr << "time ack received" << ack_found->timestamp_ack_received << endl;
      cerr << "time RTT: " << rtt << "seqno: " << endl;
    if (rtt > threshold) {
      if (the_window_size > min_window)
        the_window_size *= 0.5;
    } else {
      //the_window_size += 1;
      num_recvd_acks += 1;
      if (num_recvd_acks >= the_window_size) {
        the_window_size += 1;
        num_recvd_acks = 0;
      }
    }
    acks_info = oldest_ack->next_ack;
    if (acks_info == NULL)
      newest_ack_received = NULL;
    free(oldest_ack);

    if ( debug_ ) {
      cerr << "At time " << timestamp_ms()
           << " window size is " << the_window_size << endl;
    }
  }

  if ( debug_ ) {
//    cerr << "At time " << timestamp_ms()
//	 << " window size is " << the_window_size << endl;
  }

  return the_window_size;
}

/* A datagram was sent */
void Controller::datagram_was_sent( const uint64_t sequence_number,
				    /* of the sent datagram */
				    const uint64_t send_timestamp )
                                    /* in milliseconds */
{
/*
  datagram_sent_info *datagram = new datagram_sent_info;
  datagram->send_timestamp = send_timestamp;
  datagram->sequence_number = sequence_number;
*/  /* Oldest datagrams are at front of list, newest datagrams
     are at back of list */
/*  if (newest_datagram_sent == NULL) {
    newest_datagram_sent = datagram;
    datagrams_info = newest_datagram_sent;
  } else {
    newest_datagram_sent->next_datagram = datagram;
    newest_datagram_sent = datagram;
  }
  datagram->next_datagram = NULL;
*/ 
  /* Default: take no action */

//  if ( debug_ ) {
//    cerr << "At time " << send_timestamp
//	 << " sent datagram " << sequence_number << endl;
//  }
  (void)(sequence_number);
  (void)(send_timestamp);
  //sequence_number UNUSED;
  //send_timestamp UNUSED;
}

/* An ack was received */
void Controller::ack_received( const uint64_t sequence_number_acked,
			       /* what sequence number was acknowledged */
			       const uint64_t send_timestamp_acked,
			       /* when the acknowledged datagram was sent (sender's clock) */
			       const uint64_t recv_timestamp_acked,
			       /* when the acknowledged datagram was received (receiver's clock)*/
			       const uint64_t timestamp_ack_received )
                               /* when the ack was received (by sender) */
{
  ack_received_info *ack = new ack_received_info;
  ack->sequence_number_acked = sequence_number_acked;
  ack->send_timestamp_acked = send_timestamp_acked;
  ack->recv_timestamp_acked = recv_timestamp_acked;
  ack->timestamp_ack_received = timestamp_ack_received;
/*
  acks_map[sequence_number_acked] = ack;
*/

  if (newest_ack_received == NULL) {
    newest_ack_received = ack;
    acks_info = newest_ack_received;
  } else {
    newest_ack_received->next_ack = ack;
    newest_ack_received = ack;
  }
  ack->next_ack = NULL;
  //ack->next_ack = acks_info;
  

  /* Default: take no action */

  if ( debug_ ) {
//    cerr << "At time " << timestamp_ack_received
//	 << " received ack for datagram " << sequence_number_acked
//	 << " (send @ time " << send_timestamp_acked
//	 << ", received @ time " << recv_timestamp_acked << " by receiver's clock)"
//	 << endl;
  }
}

/* How long to wait (in milliseconds) if there are no acks
   before sending one more datagram */
unsigned int Controller::timeout_ms( void )
{
  return 1000; /* timeout of one second */
}
