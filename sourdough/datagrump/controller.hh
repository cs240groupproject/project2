#ifndef CONTROLLER_HH
#define CONTROLLER_HH

#include <cstdint>
//#include <map>
/*
struct datagram_sent_info
{
  uint64_t sequence_number;
  uint64_t send_timestamp;
  struct datagram_sent_info *next_datagram;
};
*/

struct ack_received_info
{
  uint64_t sequence_number_acked;
  uint64_t send_timestamp_acked;
  uint64_t recv_timestamp_acked;
  uint64_t timestamp_ack_received;
  //struct ack_received_info *prev_ack;
  struct ack_received_info *next_ack;
};

/* Congestion controller interface */

class Controller
{
private:
  bool debug_; /* Enables debugging output */

  /* Add member variables here */
  //struct datagram_sent_info *datagrams_info;
  /* Pointer to the most recent datagram sent */
  //struct datagram_sent_info *newest_datagram_sent;

  //std::map<uint64_t, struct ack_received_info *> acks_map; 
  struct ack_received_info *acks_info;
  /* Pointer to the most recent ack received */
  struct ack_received_info *newest_ack_received;
  unsigned int the_window_size;
  unsigned int num_recvd_acks;
public:
  /* Public interface for the congestion controller */
  /* You can change these if you prefer, but will need to change
     the call site as well (in sender.cc) */

  /* Default constructor */
  Controller( const bool debug );
  Controller( const Controller &obj );
  Controller& operator=( const Controller &other );

  /* Get current window size, in datagrams */
  unsigned int window_size( void );

  /* A datagram was sent */
  void datagram_was_sent( const uint64_t sequence_number,
			  const uint64_t send_timestamp );

  /* An ack was received */
  void ack_received( const uint64_t sequence_number_acked,
		     const uint64_t send_timestamp_acked,
		     const uint64_t recv_timestamp_acked,
		     const uint64_t timestamp_ack_received );

  /* How long to wait (in milliseconds) if there are no acks
     before sending one more datagram */
  unsigned int timeout_ms( void );
};

#endif
